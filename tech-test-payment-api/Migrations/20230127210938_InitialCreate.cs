﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Commerce.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    productid = table.Column<int>(name: "product_id", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    productname = table.Column<string>(name: "product_name", type: "nvarchar(max)", nullable: true),
                    productdescription = table.Column<string>(name: "product_description", type: "nvarchar(max)", nullable: true),
                    productvalue = table.Column<decimal>(name: "product_value", type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.productid);
                });

            migrationBuilder.CreateTable(
                name: "Sallers",
                columns: table => new
                {
                    sallerid = table.Column<int>(name: "saller_id", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    sallernamesaller = table.Column<string>(name: "saller_name_saller", type: "nvarchar(max)", nullable: true),
                    sallercpf = table.Column<string>(name: "saller_cpf", type: "nvarchar(max)", nullable: true),
                    salleremail = table.Column<string>(name: "saller_email", type: "nvarchar(max)", nullable: true),
                    salleractive = table.Column<bool>(name: "saller_active", type: "bit", nullable: false),
                    sallertelephone = table.Column<string>(name: "saller_telephone", type: "nvarchar(max)", nullable: true),
                    sallercreatedat = table.Column<DateTime>(name: "saller_created_at", type: "datetime2", nullable: false),
                    sallerupdatedat = table.Column<DateTime>(name: "saller_updated_at", type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sallers", x => x.sallerid);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    saleid = table.Column<int>(name: "sale_id", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    salename = table.Column<string>(name: "sale_name", type: "nvarchar(max)", nullable: true),
                    sallerid = table.Column<int>(name: "saller_id", type: "int", nullable: true),
                    productid = table.Column<int>(name: "product_id", type: "int", nullable: true),
                    saleproductvaluetotal = table.Column<decimal>(name: "sale_product_value_total", type: "decimal(18,2)", nullable: false),
                    salestatus = table.Column<int>(name: "sale_status", type: "int", nullable: false),
                    salecreatedat = table.Column<DateTime>(name: "sale_created_at", type: "datetime2", nullable: false),
                    saleupdatedat = table.Column<DateTime>(name: "sale_updated_at", type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.saleid);
                    table.ForeignKey(
                        name: "FK_Sales_Products_product_id",
                        column: x => x.productid,
                        principalTable: "Products",
                        principalColumn: "product_id");
                    table.ForeignKey(
                        name: "FK_Sales_Sallers_saller_id",
                        column: x => x.sallerid,
                        principalTable: "Sallers",
                        principalColumn: "saller_id");
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "product_id", "product_description", "product_name", "product_value" },
                values: new object[,]
                {
                    { 1, "Carne Bovina para Churrasco", "Carne Bovina", 12m },
                    { 2, "Batata Palha - 1Kg", "Batata Palha - 1Kg", 29m },
                    { 3, "Queijo Parmesão - 5,5Kg", "Queijo Parmesão - 5,5Kg", 300m }
                });

            migrationBuilder.InsertData(
                table: "Sallers",
                columns: new[] { "saller_id", "saller_active", "saller_cpf", "saller_created_at", "saller_email", "saller_name_saller", "saller_telephone", "saller_updated_at" },
                values: new object[,]
                {
                    { 1, true, "000.000.000-00", new DateTime(2023, 1, 27, 21, 9, 37, 938, DateTimeKind.Utc).AddTicks(3033), "string", "Person", "0000000-0000", new DateTime(2023, 1, 27, 21, 9, 37, 938, DateTimeKind.Utc).AddTicks(3038) },
                    { 2, true, "000.000.000-00", new DateTime(2023, 1, 27, 21, 9, 37, 938, DateTimeKind.Utc).AddTicks(3042), "string", "Roberto", "0000000-0000", new DateTime(2023, 1, 27, 21, 9, 37, 938, DateTimeKind.Utc).AddTicks(3043) },
                    { 3, true, "000.000.000-00", new DateTime(2023, 1, 27, 21, 9, 37, 938, DateTimeKind.Utc).AddTicks(3046), "string", "Gabriela", "0000000-0000", new DateTime(2023, 1, 27, 21, 9, 37, 938, DateTimeKind.Utc).AddTicks(3047) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sales_product_id",
                table: "Sales",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_saller_id",
                table: "Sales",
                column: "saller_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sales");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Sallers");
        }
    }
}
